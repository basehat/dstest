package puglife.dis.test.acastro.com.puglife;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener {

    private GridView pugGrid;
    private Pugs pugList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pugList = new Pugs(this, R.layout.pug);

        pugGrid = (GridView)findViewById(R.id.gridview);
        pugGrid.setAdapter(pugList);
        pugGrid.setOnScrollListener(this);
        pugGrid.setOnItemClickListener(pugList);
        //load the first page asynchronously
        pugList.load(0);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.d(getClass().getName(), "Scrolling, first visible: " + firstVisibleItem);

        if(firstVisibleItem >= (pugGrid.getCount()/2)) {
            Log.d(getClass().getName(), "Pre-loading next page of image uris");
            pugList.loadNext();
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}
