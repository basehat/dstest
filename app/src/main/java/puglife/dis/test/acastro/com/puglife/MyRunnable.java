package puglife.dis.test.acastro.com.puglife;

/**
 * Created by baseh on 3/3/2016.
 */
abstract class MyRunnable implements Runnable {

    protected Object params;

    public MyRunnable(Object o){
        params = o;
    }

    public void setParams(Object o){ params = o;}

}

