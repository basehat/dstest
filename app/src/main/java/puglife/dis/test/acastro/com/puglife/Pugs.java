package puglife.dis.test.acastro.com.puglife;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by baseh on 3/3/2016.
 */
public class Pugs extends ArrayAdapter implements AdapterView.OnItemClickListener {

    private Context _ctx;
    private int _page = 0;
    private int _pugsPerPage = 0;
    private String _apiEndpoint;
    private Thread _apiThread;
    private RequestQueue _queue;
    private LayoutInflater _viewParser;
    private boolean _isLoading = false;
    ClipboardManager _clipboard;

    public Pugs(Context context, int resource) {
        super(context, resource);

        String pugcount = context.getString(R.string.pugs_per_page);

        this._queue = Volley.newRequestQueue(context);
        this._apiEndpoint = context.getString(R.string.pug_endpoint) + pugcount;
        this._pugsPerPage = Integer.valueOf(pugcount);
        this._viewParser = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this._clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        this._ctx = context;
    }

    public void loadNext() {
        _page = _page++;
        this.load(_page);
    }

    public void load(int page) {

        if(_isLoading) {
            return;
        }

        _isLoading = true;

        Log.d(this.getClass().getName(), "Loading page: " + page);

        _page = page;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(this.getClass().getName(), "Fetching pug list for page: " + _page);
                int offset = _page * _pugsPerPage;
                String uri = Pugs.this._apiEndpoint + Pugs.this._pugsPerPage + "&start=" + offset;
                Log.d(this.getClass().getName(), "Uri: " + uri);

                StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(this.getClass().getName(), "Loaded json response, len: " + response.length());

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);
                                    JSONArray pugsArray = jsonResponse.getJSONArray("pugs");

                                    for(int i = 0;i< pugsArray.length(); i++) {
                                        Pug p = new Pug();
                                        p.id = i;
                                        p.image_uri = pugsArray.getString(i);
                                        Pugs.this.add(p);
                                        Log.d(this.getClass().getName(), "Added pug #" + p.id);
                                    }

                                    Pugs.this.notifyDataSetChanged();

                                } catch (JSONException e) {
                                    Log.e(this.getClass().getName(), "Exception parsing json response!");
                                    e.printStackTrace();
                                }

                                _isLoading = false;

                                return;

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(this.getClass().getName(), "Exception fetching json list of items!");
                        error.printStackTrace();
                    }
                });

                stringRequest.setShouldCache(false);
                _queue.add(stringRequest);
            }
        }).run();
    }

    private class PugDownloader extends AsyncTask {
        @Override
        protected Void doInBackground(Object[] params) {
            int position = (int)params[0];
            final Pug p = (Pug)getItem(position);
            final ImageView v = (ImageView)params[1];

            Log.d(getClass().getName(), "downloading image: " + p.image_uri);

            try {
                URL thumb_u = new URL(p.image_uri);
                //openStream causes the remote image to be downloaded by the sdk without caching
                //or complex error handling
                final Drawable thumb = Drawable.createFromStream(thumb_u.openStream(), "src");

                ((Activity)_ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        v.setImageDrawable(thumb);
                       /* v.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                ClipData clip = android.content.ClipData.newPlainText("image uri", p.image_uri);
                                _clipboard.setPrimaryClip(clip);
                                return true;
                            }
                        });*/
                        v.invalidate();
                    }
                });

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d(this.getClass().getName(), "Downloading and rendering pug #" + position);

        View vi = convertView;

        if (vi == null) {
            vi = _viewParser.inflate(R.layout.pug, null);
        }

        ImageView iv = (ImageView)vi.findViewById(R.id.pug_image);
        iv.setImageDrawable(_ctx.getResources().getDrawable(R.drawable.loading));
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iv.setMaxHeight(90);


        Object[] params = new Object[2];
        params[0] = position;
        params[1] = iv;

        new PugDownloader().execute(params);

        return vi;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(getClass().getName(), "Showing image: " + position);
        Pug p = (Pug)getItem(position);
        Intent i = new Intent(_ctx, PugviewActivity.class);

        Log.d(this.getClass().getName(), "opening details activity for: " + p);

        i.putExtra("pug", p);

        _ctx.startActivity(i);

    }
}
