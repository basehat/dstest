package puglife.dis.test.acastro.com.puglife;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class PugviewActivity extends AppCompatActivity {

    private Pug _currentPug;

    private class DownloadImageTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Log.d(getClass().getName(), "downloading image: " + _currentPug.image_uri);

            try {
                URL thumb_u = new URL(_currentPug.image_uri);
                //openStream causes the remote image to be downloaded by the sdk without caching
                //or complex error handling
                final Drawable thumb_d = Drawable.createFromStream(thumb_u.openStream(), "src");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageView image = (ImageView) findViewById(R.id.image_fullsize);
                        image.setImageDrawable(thumb_d);
                        image.invalidate();
                    }
                });

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pugview);

        ImageView mainImage = (ImageView)findViewById(R.id.image_fullsize);
        Button btnNext = (Button)findViewById(R.id.btn_next);
        Button btnPrev = (Button)findViewById(R.id.btn_prev);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _currentPug = (Pug)getIntent().getSerializableExtra("pug");

        //download and display image asynchronously
        new DownloadImageTask().execute();
    }
}
